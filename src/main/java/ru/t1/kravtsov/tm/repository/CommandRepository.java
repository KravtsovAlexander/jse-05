package ru.t1.kravtsov.tm.repository;

import ru.t1.kravtsov.tm.api.ICommandRepository;
import ru.t1.kravtsov.tm.constant.ArgumentConst;
import ru.t1.kravtsov.tm.constant.TerminalConst;
import ru.t1.kravtsov.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    private static final Command HELP = new Command(
            TerminalConst.HELP, ArgumentConst.HELP, "Display list of terminal commands."
    );

    private static final Command VERSION = new Command(
            TerminalConst.VERSION, ArgumentConst.VERSION, "Display program version."
    );

    private static final Command ABOUT = new Command(
            TerminalConst.ABOUT, ArgumentConst.ABOUT, "Display developer info."
    );

    private static final Command COMMANDS = new Command(
            TerminalConst.COMMANDS, ArgumentConst.COMMANDS, "Display application commands."
    );

    private static final Command ARGUMENTS = new Command(
            TerminalConst.ARGUMENTS, ArgumentConst.ARGUMENTS, "Display application arguments."
    );

    private static final Command EXIT = new Command(
            TerminalConst.EXIT, null, "Close application."
    );

    private static final Command[] TERMINAL_COMMANDS = new Command[]{
            HELP, VERSION, ABOUT, COMMANDS, ARGUMENTS, EXIT
    };

    @Override
    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
