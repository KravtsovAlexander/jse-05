package ru.t1.kravtsov.tm.constant;

public final class TerminalConst {

    public static final String VERSION = "version";

    public static final String HELP = "help";

    public static final String ABOUT = "about";

    public static final String EXIT = "exit";

    public static final String ARGUMENTS = "arguments";

    public static final String COMMANDS = "commands";

}
